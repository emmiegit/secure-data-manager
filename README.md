# secure-data-manager
A simple text encryption program I wrote for my personal use.

**Note:** I'm still transitioning to Git, so there are parts of the repo that are undocumented until then.

## Building
This project is built with Maven. If you're building for a Windows platform, then invoke:
<pre>mvn windows</pre>
Otherwise run the following to create a `jar` file and shell scripts:
<pre>mvn package</pre>

