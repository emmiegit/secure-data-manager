[Setup]
AppName=Secure Text Manager
AppVersion=1.3.9
DefaultDirName={pf}\Secure Text Manager
DisableProgramGroupPage=yes
OutputDir=dist\
OutputBaseFilename=SecureTextManager_Setup_1.3.9
LicenseFile=license.txt

[Files]
Source: "dist\secure.exe";       DestDir: "{app}";   Attribs: readonly
Source: "checksums.txt";         DestDir: "{app}";   Attribs: readonly
Source: "license.txt";           DestDir: "{app}";   Attribs: readonly
 
[Icons]
Name: "{commonprograms}\Secure Text Manager\Secure Text Manager"; Filename: "{app}\secure.exe"; Comment: Opens the Secure Text Manager.;                      Tasks: quicklaunchicon
Name: "{commonprograms}\Secure Text Manager\Uninstall";           Filename: "{uninstallexe}";   Comment: Removes the Secure Text Manager from your computer.; Tasks: quicklaunchicon
Name: "{commondesktop}\Secure Text Manager";                      Filename: "{app}\secure.exe"; Comment: Opens the Secure Text Manager.;                      Tasks: desktopicon

[Tasks]
Name: desktopicon;      Description: "Create a &desktop icon";      GroupDescription: Create shortcuts:
Name: quicklaunchicon;  Description: "Create a &Quick Launch icon"; GroupDescription: Create shortcuts:
Name: associate;        Description: "&Associate .crypt files";     GroupDescription: File extensions:

[Registry]
Root: "HKCR"; Subkey: ".crypt";                               ValueType: string; ValueData: "SecureTextManager";            Flags: uninsdeletevalue; Tasks: associate
Root: "HKCR"; Subkey: "SecureTextManager";                    ValueType: string; ValueData: "Secure Text Document";         Flags: uninsdeletekey;   Tasks: associate
Root: "HKCR"; Subkey: "SecureTextManager\DefaultIcon";        ValueType: string; ValueData: "{app}\resources\cryptdoc.ico"; Flags: uninsdeletekey;   Tasks: associate
Root: "HKCR"; Subkey: "SecureTextManager\shell\open\command"; ValueType: string; ValueData: """{app}\secure.exe"" ""%1""";  Flags: uninsdeletekey;   Tasks: associate

[UninstallDelete]
Type: files;  Name: "{app}\secure.exe"
Type: files;  Name: "{app}\checksums.txt"
Type: files;  Name: "{app}\license.txt"
Type: files;  Name: "{app}\config.properties"
