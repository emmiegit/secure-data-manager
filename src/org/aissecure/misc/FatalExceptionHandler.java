package org.aissecure.misc;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.Thread.UncaughtExceptionHandler;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class FatalExceptionHandler implements UncaughtExceptionHandler {
    public static final FatalExceptionHandler INSTANCE = new FatalExceptionHandler();
    
    private FatalExceptionHandler() {
    }
    
    @Override
    public void uncaughtException(Thread thread, final Throwable throwable) {
        final String threadInfo = String.format("Exception in %s thread #%d \"%s\".",
                thread.getState().toString().toLowerCase(), thread.getId(), thread.getName());
        
        System.err.println(threadInfo);
        throwable.printStackTrace();
        
        @SuppressWarnings("serial")
        class ErrorDisplayFrame extends JFrame {
            private final JTextArea trace;
            private final JButton quit;
            
            public ErrorDisplayFrame() {
                super("Fatal error");
                
                trace = new JTextArea();
                trace.setEditable(false);
                trace.setText(String.format(
                    "Th%s has encountered a fatal error and must terminate.\n"
                  + "Please report this error to the developer:\n\n%s\n%s",
                    throwable instanceof Error ? "e Java Virtual Machine" : "is program",
                    threadInfo, ExceptionUtils.getStackTrace(throwable)));
                
                quit = new JButton("Quit");
                quit.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent evt) {
                        try(FileWriter writer = new FileWriter("errors.log")) {
                            writer.append(trace.getText());
                        } catch(IOException ex) {
                            System.err.println("Failed to write error log to file.");
                        }
                        
                        System.exit(1);
                    }
                });
                
                setContentPane(new JPanel() {{
                    setLayout(new BorderLayout());

                    add(new JScrollPane(trace), BorderLayout.CENTER);
                    add(new JPanel() {{
                        setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
                        
                        add(Box.createHorizontalGlue());
                        add(quit);
                        add(Box.createHorizontalGlue());
                    }}, BorderLayout.SOUTH);
                }});
                
                setSize(400, 600);
                setLocationByPlatform(true);
            }
        }
        
        new ErrorDisplayFrame().setVisible(true);
    }
}
