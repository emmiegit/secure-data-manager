/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.misc;

import java.io.InvalidObjectException;
import java.io.ObjectInputValidation;
import java.io.Serializable;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public final class Arguments implements Serializable, ObjectInputValidation {
    private static final long serialVersionUID = 7124280732943626810L;
    
    private final String[] args;
    
    private final String[] flags;
    private final Set<String> uncheckedFlags;
    private final Map<String, String> values;
    
    private final char[] shortFlags;
    private final Set<Character> uncheckedShortFlags;
    private final Map<Character, String> shortValues;
    
    public Arguments(String[] args) {
        this.args = args;
        
        uncheckedFlags = new HashSet<>();
        uncheckedShortFlags = new HashSet<>();
        values = new HashMap<>();
        shortValues = new HashMap<>();
        
        for(String arg : args) {
            if(arg.startsWith("--")) {
                if(arg.contains("=")) {
                    Entry<String, String> pair = split(arg.substring(2));
                    values.put(pair.getKey(), pair.getValue());
                    uncheckedFlags.add(pair.getKey());
                } else {
                    uncheckedFlags.add(arg.substring(2));
                }
            } else if(arg.startsWith("-")) {
                if(arg.contains("=")) {
                    Entry<String, String> pair = split(arg.substring(1));
                    
                    if(!pair.getKey().isEmpty()) {
                        char key = Character.toLowerCase(pair.getKey().charAt(0));
                        shortValues.put(key, pair.getValue());
                        uncheckedShortFlags.add(key);
                    }
                } else {
                    for(char c : arg.substring(1).toCharArray())
                        uncheckedShortFlags.add(Character.toLowerCase(c));
                }
            }
        }
        
        flags = new String[uncheckedFlags.size()];
        
        Object[] array = uncheckedFlags.toArray();
        for(int i = 0; i < flags.length; i++)
            flags[i] = (String)array[i];
        
        shortFlags = new char[uncheckedShortFlags.size()];
        
        array = uncheckedShortFlags.toArray();
        for(int i = 0; i < shortFlags.length; i++)
            shortFlags[i] = (char)array[i];
    }
    
    private static Entry<String, String> split(String arg) {
        int index = arg.indexOf('=');
        return new SimpleImmutableEntry<>(arg.substring(0, index), arg.substring(index + 1));
    }
    
    public boolean isEmpty() {
        return args.length == 0;
    }
    
    public String getFirst() {
        return args[0];
    }
    
    public String getLast() {
        return args[args.length - 1];
    }
    
    public String getArg(int index) {
        return args[index];
    }
    
    public boolean contains(char flag) {
        flag = Character.toLowerCase(flag);
        
        for(int i = 0; i < shortFlags.length; i++) {
            if(shortFlags[i] == flag) {
                uncheckedShortFlags.remove(flag);
                return true;
            }
        }
        return false;
    }
    
    public boolean contains(String flag) {
        for(String f : flags) {
            if(f.equalsIgnoreCase(flag)) {
                uncheckedFlags.remove(flag);
                return true;
            }
        }
        return false;
    }
    
    public boolean containsLiteral(String arg) {
        for(String a : args)
            if(a.equalsIgnoreCase(arg))
                return true;
        return false;
    }
    
    public String[] getRawArgs() {
        return args.clone();
    }
    
    public String[] getFlags() {
        return flags.clone();
    }
    
    public char[] getShortFlags() {
        return shortFlags.clone();
    }
    
    public Set<String> getAllUncheckedFlags() {
        Set<String> result = new HashSet<>(uncheckedFlags);
        
        for(char flag : uncheckedShortFlags)
            result.add(Character.toString(flag));
        
        return Collections.unmodifiableSet(result);
    }
    
    public Set<String> getUncheckedFlags() {
        return Collections.unmodifiableSet(uncheckedFlags);
    }
    
    public Set<Character> getUncheckedShortFlags() {
        return Collections.unmodifiableSet(uncheckedShortFlags);
    }
    
    public Map<String, String> getAllValues() {
        Map<String, String> result = new HashMap<>(values);
        
        for(char flag : shortValues.keySet())
            result.put(Character.toString(flag), shortValues.get(flag));
        
        return Collections.unmodifiableMap(result);
    }
    
    public Map<String, String> getValues() {
        return Collections.unmodifiableMap(values);
    }
    
    public Map<Character, String> getShortValues() {
        return Collections.unmodifiableMap(shortValues);
    }
    
    public boolean hasRawArg(String arg) {
        for(String s : args)
            if(s.equalsIgnoreCase(arg))
                return true;
        return false;
    }
    
    public boolean hasFlag(String flag) {
        for(String s : flags)
            if(s.equalsIgnoreCase(flag))
                return true;
        return false;
    }
    
    public boolean hasShortFlag(char flag) {
        flag = Character.toLowerCase(flag);
        
        for(char c : shortFlags)
            if(c == flag)
                return true;
        return false;
    }
    
    public boolean hasAnyUncheckedFlag(String flag) {
        if(flag.length() == 1)
            return hasUncheckedShortFlag(flag.charAt(0)) || hasUncheckedFlag(flag);
        
        return hasUncheckedFlag(flag);
    }
    
    public boolean hasUncheckedFlag(String flag) {
        for(String s : uncheckedFlags)
            if(s.equalsIgnoreCase(flag))
                return true;
        return false;
    }
    
    public boolean hasUncheckedShortFlag(char flag) {
        return uncheckedShortFlags.contains(Character.toLowerCase(flag));
    }
    
    public boolean hasAnyValue(String flag) {
        if(flag.length() == 1)
            return hasShortValue(flag.charAt(0)) || hasValue(flag);
        
        return hasValue(flag);
    }
    
    public boolean hasValue(String flag) {
        for(String s : values.keySet())
            if(s.equalsIgnoreCase(flag))
                return true;
        return false;
    }
    
    public boolean hasShortValue(char flag) {
        return shortValues.containsKey(Character.toLowerCase(flag));
    }
    
    public void resetFlags() {
        uncheckedFlags.clear();
        
        for(String flag : flags)
            uncheckedFlags.add(flag);
        
        uncheckedShortFlags.clear();
        
        for(char flag : shortFlags)
            uncheckedShortFlags.add(flag);
    }
    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder();
        
        for(int i = 0; i < args.length; i++) {
            result.append(args[i]);
            
            if(i < args.length - 1)
                result.append(' ');
        }
        
        return result.toString();
    }
    
    @Override
    public void validateObject() throws InvalidObjectException {
        if(flags == null)
            throw new InvalidObjectException("Internal flags array is null.");
        
        for(String flag : flags)
            if(flag == null)
                throw new InvalidObjectException("Flag in the array is null.");
        
        if(uncheckedFlags == null)
            throw new InvalidObjectException("Internal unchecked flags set is null.");
        
        if(uncheckedFlags.contains(null))
            throw new InvalidObjectException("Unchecked flags set contains null element.");
        
        if(shortFlags == null)
            throw new InvalidObjectException("Internal short flags array is null.");
        
        if(uncheckedShortFlags == null)
            throw new InvalidObjectException("");
        
        if(uncheckedShortFlags.contains(null))
            throw new InvalidObjectException("Unchecked short flags set contains null element.");
        
        if(args == null)
            throw new InvalidObjectException("Internal argument array is null.");
        
        for(String arg : args)
            if(arg == null)
                throw new InvalidObjectException("Argument in the array is null.");
    }
}
