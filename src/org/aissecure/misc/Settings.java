/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.misc;

import java.awt.Dimension;
import java.awt.Font;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.TreeMap;

import javax.swing.JOptionPane;

@SuppressWarnings("serial")
public final class Settings {
    private static final File path = new File(System.getProperty("user.dir") + "/.aissec.properties");
    private static final NavigableMap<String, String> defaults;
    
    static {
        try {
            path.createNewFile();
        } catch(IOException ex) {
        }
        
        defaults = new TreeMap<String, String>() {{
            put("hash-difficulty", "12");
            put("cipher-saltsize", "64");
            put("cipher-iterations", "65536");
            put("editor-width", "400");
            put("editor-height", "500");
            put("last-opened-file", "");
            put("save-directory", System.getProperty("user.home") + "/Secure Documents");
            put("do-file-encrypt-check", "true");
            put("do-close-confirmation-always", "false");
            put("show-encrypt-confirmation", "true");
            put("show-display-confirmation", "true");
            put("show-unlimited-crypto", "true");
            put("suppress-readonly-warning", "false");
            put("font-face", Font.SANS_SERIF);
            put("font-style", Integer.toString(Font.PLAIN));
            put("font-size", "12");
            put("javax-look-and-feel", "default");
        }};
    }
    
    private static Settings instance;
    
    public static Settings get() {
        if(instance == null) {
            try(BufferedReader input = new BufferedReader(new FileReader(path))) {
                NavigableMap<String, String> values = new TreeMap<>();
                String line;
                while((line = input.readLine()) != null) {
                    if(!line.isEmpty() && !line.startsWith("#")) {
                        Entry<String, String> pair = parseLine(line);
                        values.put(pair.getKey(), pair.getValue());
                    }
                }
                
                instance = new Settings(values);
                instance.check();
            } catch(IOException ex) {
                instance = new Settings(defaults);
            }
        }
        
        return instance;
    }
    
    private static Entry<String, String> parseLine(String line) {
        String[] parts = line.split("=");
        StringBuilder value = new StringBuilder();
        
        for(int i = 1; i < parts.length; i++) {
            value.append(parts[i]);
            
            if(i < parts.length - 1)
                value.append('=');
        }
        
        return new SimpleImmutableEntry<>(parts[0], value.toString());
    }
    
    public static void save() {
        if(instance != null)
            instance.store();
    }
    
    private final NavigableMap<String, String> values;
    private final Dimension size;
    private Font font;
    
    private Settings(NavigableMap<String, String> properties) {
        values = properties;
        size = new Dimension(getInt("editor-width"), getInt("editor-height"));
        font = new Font(values.get("font-face"), getInt("font-style"), getInt("font-size"));
    }
    
    private void check() {
        if(4 > getHashDifficulty() || getHashDifficulty() > 30) {
            JOptionPane.showMessageDialog(null,
                "The hash difficulty you entered was invalid. It\n"
              + "must be an integer in between 4 and 30 inclusive.\n"
              + "A difficulty of 12 is being used instead.",
                "Error in configuration file",
                JOptionPane.ERROR_MESSAGE);
            setHashDifficulty(12);
        }
        
        if(getCipherSaltSize() < 64)
            JOptionPane.showMessageDialog(null,
                "It is recommended to use a salt size of at least 64\n"
              + "bytes. Using too small of a salt size can be insecure.",
                "Warning in configuration file",
                JOptionPane.WARNING_MESSAGE);
        
        if(getCipherIterations() < 8192)
            JOptionPane.showMessageDialog(null,
                "It is recommended to do at least 8,192 cipher iterations during\n"
              + "key generation. Using too few iterations can be insecure.",
                "Warning in configuration file",
                JOptionPane.WARNING_MESSAGE);
    }
    
    public boolean getEncryptConfirmation() {
        return getBoolean("show-encrypt-confirmation");
    }
    
    public boolean getDisplayConfirmation() {
        return getBoolean("show-display-confirmation");
    }
    
    public boolean getReadonlyWarning() {
        return getBoolean("show-readonly-warning");
    }
    
    public boolean doFileEncryptionCheck() {
        return getBoolean("do-file-encrypt-check");
    }
    
    public boolean doFileCloseConfirmation() {
        return getBoolean("do-close-confirmation-always");
    }
    
    public boolean suppressUnlimitedCrypto() {
        return getBoolean("suppress-unlimited-crypto");
    }
    
    public File getDirectory() {
        String dir = values.containsKey("save-directory")
                   ? values.get("save-directory")
                   : defaults.get("save-directory");
        
        return new File(dir);
    }
    
    public File getLastOpenedFile() {
        String dir = values.containsKey("last-opened-file")
                   ? values.get("last-opened-file")
                   : defaults.get("last-opened-file");
        
        return new File(dir);
    }
    
    public void setLastOpenedFile(File path) {
        if(path == null)
            values.put("last-opened-file", "");
        else if(path.exists())
            values.put("last-opened-file", path.toString());
    }
    
    public Dimension getSize() {
        return size;
    }
    
    public Font getFont() {
        return font;
    }
    
    public void setFont(Font font) {
        this.font = font;
    }
    
    public String getLookAndFeel() {
        return values.containsKey("javax-look-and-feel")
             ? values.get("javax-look-and-feel")
             : defaults.get("javax-look-and-feel");
    }
    
    public int getCipherSaltSize() {
        return getInt("cipher-saltsize");
    }
    
    public int getCipherIterations() {
        return getInt("cipher-iterations");
    }
    
    public int getHashDifficulty() {
        return getInt("hash-difficulty");
    }
    
    private void setHashDifficulty(int difficulty) {
        values.put("hash-difficulty", Integer.toString(difficulty));
    }
    
    private int getInt(String name) {
        try {
            int value = Integer.parseInt(values.get(name));
            
            if(value <= 0)
                throw new NumberFormatException();
            else
                return value;
        } catch(NumberFormatException ex) {
            return Integer.parseInt(defaults.get(name));
        }
    }
    
    private boolean getBoolean(String name) {
        String property =
            (values.containsKey(name)
            ? values.get(name)
            : defaults.get(name)).toLowerCase();
        
        return property.equals("true") ||
               property.equals("t")    ||
               property.equals("yes")  ||
               property.equals("y")    ||
               property.equals("sure") ||
               property.equals("why not");
    }
    
    void store() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                values.put("save-directory", getDirectory().toString());
                values.put("editor-width", Integer.toString(size.width));
                values.put("editor-height", Integer.toString(size.height));
                values.put("font-face", font.getName());
                values.put("font-style", Integer.toString(font.getStyle()));
                values.put("font-size", Integer.toString(font.getSize()));
                
                for(String key : defaults.keySet())
                    if(!values.containsKey(key))
                        values.put(key, defaults.get(key));
                
                try(PrintStream output = new PrintStream(new FileOutputStream(path))) {
                    output.println("# All configurable settings for this application are listed below:");
                    output.println("# Note that \"hash-difficulty\" must be between 4 and 30.");
                    output.println();
                    
                    for(String key : values.navigableKeySet())
                        output.println(String.format("%s=%s", key, values.get(key)));
                    
                    output.println();
                } catch(IOException ex) {
                    JOptionPane.showMessageDialog(null,
                        String.format("Unable to save application settings\nto \"%s\".", path.getAbsolutePath()),
                        "Error saving settings",
                        JOptionPane.ERROR_MESSAGE);
                }
            }
        }).start();
    }
    
    @Override
    public String toString() {
        return values.toString();
    }
}
