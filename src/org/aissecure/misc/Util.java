/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.misc;

import org.aissecure.Launcher;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Field;
import java.security.Permission;
import java.security.PermissionCollection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;


public final class Util {
    private Util() {}
    
    public static final int MIN_PASSWORD_LENGTH = 7;
    private static final String[] passwords;
    private static boolean unlimitedCrypto;
    
    static {
        InputStream stream = Launcher.class.getResourceAsStream("/passwords.txt");
        String[] pwds;
        
        try(BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"))) {
            List<String> buffer = new ArrayList<>(10000);
            String line;
            
            while((line = reader.readLine()) != null)
                buffer.add(line);
            
            pwds = buffer.toArray(new String[buffer.size()]);
        } catch(IOException | NullPointerException ex) {
            pwds = new String[0];
        }
        
        passwords = pwds;
    }
    
    public static void removeCryptographyRestrictions() {
        if(!"Java(TM) SE Runtime Environment".equals(System.getProperty("java.runtime.name"))) {
            unlimitedCrypto = true;
            return;
        } else if(Settings.get().suppressUnlimitedCrypto()) {
            unlimitedCrypto = false;
            return;
        }
        
        try {
            /*
             * Do the following, but with reflection to bypass access checks:
             *
             * JceSecurity.isRestricted = false;
             * JceSecurity.defaultPolicy.perms.clear();
             * JceSecurity.defaultPolicy.add(CryptoAllPermission.INSTANCE);
             */
            final Class<?> jceSecurity = Class.forName("javax.crypto.JceSecurity");
            final Class<?> cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions");
            final Class<?> cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission");

            final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted");
            isRestrictedField.setAccessible(true);
            isRestrictedField.set(null, false);

            final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy");
            defaultPolicyField.setAccessible(true);
            final PermissionCollection defaultPolicy = (PermissionCollection)defaultPolicyField.get(null);

            final Field perms = cryptoPermissions.getDeclaredField("perms");
            perms.setAccessible(true);
            ((Map<?, ?>)perms.get(defaultPolicy)).clear();

            final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE");
            instance.setAccessible(true);
            defaultPolicy.add((Permission)instance.get(null));

            unlimitedCrypto = true;
        } catch(ClassNotFoundException | IllegalAccessException | IllegalArgumentException | NoSuchFieldException | SecurityException ex) {
            unlimitedCrypto = false;
            JOptionPane.showMessageDialog(null,
                "Unlimited cryptography is restricted on this computer.\n\n"
              + "In order to use 192 or 256-bit keys, the Java Unlimited Strength\n"
              + "Jurisdiction Policy Files must be installed in the JDK,\n"
              + "specifically at {jdk}/jre/lib/security. They can be downloaded\n"
              + "from Oracle's website. Be sure to select the proper version.",
                "Large key sizes not supported",
                JOptionPane.WARNING_MESSAGE);
        }
    }
    
    public static boolean hasUnlimitedCrypto() {
        return unlimitedCrypto;
    }
    
    public static char[] getPassword(JFrame parent) {
        return getPassword(parent, "Enter the password for this save file:");
    }
    
    public static char[] getPassword(JFrame parent, String label) {
        return new PasswordDialog(parent, label).getPassword();
    }
    
    @SuppressWarnings("serial")
    private static final class PasswordDialog extends JDialog {
        private final JPanel panel;
        private final JPasswordField pf;
        private final JButton ok, cancel;
        private char[] password;
        
        public PasswordDialog(JFrame parent, String label) {
            super(parent, "Password Entry");
            panel = new JPanel();
            pf = new JPasswordField(10);
            ok = new JButton("OK");
            cancel = new JButton("Cancel");
            
            ok.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    PasswordDialog.this.dispose();
                    synchronized(PasswordDialog.this) {
                        password = pf.getPassword();
                        PasswordDialog.this.notify();
                    }
                }
            });
            
            cancel.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent evt) {
                    PasswordDialog.this.dispose();
                    synchronized(PasswordDialog.this) {
                        password = null;
                        PasswordDialog.this.notify();
                    }
                }
            });
            
            panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
            panel.add(new JLabel(label));
            panel.add(Box.createVerticalStrut(3));
            panel.add(pf);
            panel.add(Box.createVerticalStrut(3));
            panel.add(new JPanel() {{
                setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
                add(ok);
                add(Box.createHorizontalStrut(3));
                add(cancel);
            }});
            
            add(panel);
            pack();
            pf.requestFocusInWindow();
        }
        
        @Override
        protected void dialogInit() {
            super.dialogInit();
            setVisible(true);
        }
        
        public char[] getPassword() {
            synchronized(this) {
                try {
                    this.wait();
                } catch(InterruptedException ex) {
                }
                
                return password;
            }
        }
    }
    
    public static boolean isCommonPassword(char[] password) {
        for(String pwd : passwords)
            if(Arrays.equals(pwd.toCharArray(), password))
                return true;
        return false;
    }
    
    public static String getExceptionReason(Throwable throwable) {
        return throwable.getMessage() == null
             ? throwable.getClass().getName()
             : Character.toUpperCase(throwable.getMessage().charAt(0)) + throwable.getMessage().substring(1);
    }
}
