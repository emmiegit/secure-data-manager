/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.crypt;

import org.aissecure.misc.IncompatibleVersionException;
import org.aissecure.misc.Settings;
import org.aissecure.misc.Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InvalidObjectException;
import java.io.ObjectInput;
import java.io.ObjectInputStream;
import java.io.ObjectInputValidation;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignedObject;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;

import javax.crypto.Cipher;
import javax.crypto.SealedObject;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class SecureData implements Serializable, ObjectInputValidation {
    private static final long serialVersionUID = 7239138032262850714L;
    
    public static final int AES_KEY_SIZE = 256;
    public static final int DSA_KEY_SIZE = 1024;
    public static final short VERSION_ID = 1;
    
    public static SecureData fromFile(File path)
            throws IOException, ClassNotFoundException, ClassCastException {
        
        try(InputStream stream = new FileInputStream(path); 
            ObjectInput input = new ObjectInputStream(stream)) {
            
            short version = input.readShort();
            
            if(version != VERSION_ID)
                return legacyVersion(version, input);
            
            PublicKey publicKey = (PublicKey)input.readObject();
            SignedObject signed = (SignedObject)input.readObject();
            
            SecureData data = (SecureData)signed.getObject();
            data.setRijndaelKeySize(AES_KEY_SIZE);
            data.setPublicKey(publicKey);
            data.setSignedObject(signed);
            return data;
        }
    }
    
    private static SecureData legacyVersion(short version, ObjectInput input)
            throws IOException, ClassNotFoundException, ClassCastException {
        
        switch(version) {
            case 0:
                PublicKey publicKey = (PublicKey)input.readObject();
                SignedObject signed = (SignedObject)input.readObject();
                
                SecureData data = (SecureData)signed.getObject();
                data.setRijndaelKeySize(128);
                data.setPublicKey(publicKey);
                data.setSignedObject(signed);
                return data;
            default:
                throw new IncompatibleVersionException(version);
        }
    }
    
    public static void encryptBinaryFile(File path, char[] password) {
        try {
            // Max array size
            if(path.length() > Integer.MAX_VALUE - 8) {
                System.out.println("This file is too large to encrypt. (Max size: 2.15 GB)");
                return;
            }
            
            if(Settings.get().doFileEncryptionCheck()) {
                try {
                    fromFile(path);
                    System.out.println("Warning: this file appears to already be encrypted");
                } catch(IOException | ClassNotFoundException | ClassCastException ex) {
                }
            }
            
            byte[] data = new byte[(int)path.length()];
            
            try(InputStream stream = new FileInputStream(path)) {
                stream.read(data);
            }

            new SecureData(password, data, CryptConfig.fromSettings()).saveTo(path);
        } catch(IOException ex) {
            System.out.println("I/O error occurred: " + Util.getExceptionReason(ex));
            System.exit(1);
        } catch(SecurityException ex) {
            System.out.println("Unable to encrypt data: " + Util.getExceptionReason(ex));
            System.exit(1);
        } catch(IllegalStateException ex) {
            System.out.println("Cipher encountered illegal state: " + Util.getExceptionReason(ex));
            System.exit(1);
        }
    }
    
    public static void decryptBinaryFile(File path, char[] password) {
        try {
            byte[] data = fromFile(path).getByteData(password);
            
            try(OutputStream stream = new FileOutputStream(path)) {
                stream.write(data);
            }
        } catch(IOException ex) {
            System.out.println("I/O error occurred: " + Util.getExceptionReason(ex));
            System.exit(1);
        } catch(ClassNotFoundException ex) {
            System.out.println("Unknown type: " + Util.getExceptionReason(ex));
            System.exit(1);
        } catch(SecurityException ex) {
            System.out.println("Unable to decrypt data: " + Util.getExceptionReason(ex));
            System.exit(1);
        } catch(IllegalStateException ex) {
            System.out.println("Cipher encountered illegal state: " + Util.getExceptionReason(ex));
            System.exit(1);
        }
    }
    
    private Hash hash;
    private byte[] salt, iv;
    private int iterations;
    private SealedObject secure;
    private transient PublicKey publicKey;
    private transient SignedObject signed;
    private transient int keySize;
    
    public SecureData(char[] password, Serializable plaintext, CryptConfig config) throws IOException, IllegalStateException {
        iterations = config.cipherIterations;
        hash = new Hash(password, config.hashDifficulty);
        salt = new byte[config.cipherSaltSize];
        keySize = Util.hasUnlimitedCrypto() ? AES_KEY_SIZE : 128;
        new SecureRandom().nextBytes(salt);
        
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, getKey(password));
            iv = cipher.getParameters().getParameterSpec(IvParameterSpec.class).getIV();
            secure = new SealedObject(plaintext, cipher);
            
            KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
            keyPairGenerator.initialize(DSA_KEY_SIZE);
            KeyPair keyPair = keyPairGenerator.genKeyPair();
            publicKey = keyPair.getPublic();
            
            Signature signature = Signature.getInstance(keyPair.getPrivate().getAlgorithm());
            signed = new SignedObject(this, keyPair.getPrivate(), signature);
        } catch(InvalidKeyException ex) {
            ex.printStackTrace();
            
            if(!Util.hasUnlimitedCrypto())
                throw new IOException(
                    "\nIn order to use 256-bit keys, the Java Unlimited Strength\n"
                  + "Jurisdiction Policy Files must be installed in the JDK,\n"
                  + "specifically at {jdk}/jre/lib/security. They can be downloaded\n"
                  + "from Oracle's website. Be sure to select the proper version.");
            else
                throw new IllegalStateException(ex.getMessage());
        } catch(GeneralSecurityException ex) {
            ex.printStackTrace();
            throw new IllegalStateException(ex.getMessage());
        }
    }
    
    public String getPlaintext(char[] password) throws IOException, SecurityException, IllegalStateException {
        return getObject(password);
    }
    
    public byte[] getByteData(char[] password) throws IOException, SecurityException, IllegalStateException {
        return getObject(password);
    }
    
    @SuppressWarnings("unchecked")
    protected <T extends Serializable> T getObject(char[] password) throws IOException {
        if(!hash.validatePassword(password))
            throw new SecurityException("Entered password was invalid.");
        
        try {
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, getKey(password), new IvParameterSpec(iv));
            return (T)secure.getObject(cipher);
        } catch(ClassNotFoundException | ClassCastException ex) {
            ex.printStackTrace();
            throw new IllegalStateException("Sealed object is of an invalid type: " + Util.getExceptionReason(ex));
        } catch(GeneralSecurityException | IllegalArgumentException ex) {
            ex.printStackTrace();
            throw new IllegalStateException(ex.getMessage());
        }
    }
    
    private SecretKeySpec getKey(char[] password) throws InvalidKeySpecException, NoSuchAlgorithmException {
        SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        KeySpec spec = new PBEKeySpec(password, salt, iterations, keySize);
        SecretKey key = factory.generateSecret(spec);
        return new SecretKeySpec(key.getEncoded(), "AES");
    }
    
    public Hash getHash() {
        return hash;
    }
    
    public int getSaltSize() {
        return salt.length;
    }
    
    public int getIterations() {
        return iterations;
    }
    
    protected final void setRijndaelKeySize(int size) {
        assert size == 128 || size == 192 || size == 256;
        keySize = size;
    }
    
    protected final void setPublicKey(PublicKey newKey) {
        publicKey = newKey;
    }
    
    protected final void setSignedObject(SignedObject obj) {
        signed = obj;
    }
    
    public boolean signatureIsValid() {
        try {
            return signed.verify(publicKey, Signature.getInstance(publicKey.getAlgorithm()));
        } catch(GeneralSecurityException ex) {
            ex.printStackTrace();
            throw new IllegalStateException(ex.getMessage());
        }
    }
    
    // Fields are spelled out for version compatibility
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        hash = (Hash)stream.readObject();
        iv = (byte[])stream.readObject();
        salt = (byte[])stream.readObject();
        iterations = stream.readInt();
        secure = (SealedObject)stream.readObject();
        validateObject();
    }
    
    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.writeObject(hash);
        stream.writeObject(iv);
        stream.writeObject(salt);
        stream.writeInt(iterations);
        stream.writeObject(secure);
    }
    
    public void saveTo(File path) throws IOException {
        path.createNewFile();
        
        try(OutputStream stream = new FileOutputStream(path);
            ObjectOutput output = new ObjectOutputStream(stream)) {
            
            output.writeShort(VERSION_ID);
            output.writeObject(publicKey);
            output.writeObject(signed);
        }
    }
    
    @Override
    public void validateObject() throws InvalidObjectException {
        if(hash == null)
            throw new InvalidObjectException("Hash is null.");
        
        if(salt == null)
            throw new InvalidObjectException("Salt is null.");
        
        if(iv == null)
            throw new InvalidObjectException("Initialization vector is null.");
        
        if(iterations <= 0)
            throw new InvalidObjectException("Invalid number of iterations: " + iterations);
        
        if(secure == null)
            throw new InvalidObjectException("SealedObject is null.");
    }
}
