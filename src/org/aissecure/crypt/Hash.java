/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.crypt;

import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;
import java.io.InvalidObjectException;
import java.io.ObjectInputStream;
import java.io.ObjectInputValidation;
import java.io.Serializable;
import java.security.SecureRandom;

public final class Hash implements Serializable, ObjectInputValidation {
    private static final long serialVersionUID = -4349137222606184082L;
    
    private static byte[] doHash(char[] password, byte[] salt, int difficulty) {
        return new BCrypt().crypt_raw(charsToBytes(password), salt, difficulty, BCrypt.getIV());
    }
    
    private static byte[] charsToBytes(char[] chars) {
        byte[] bytes = new byte[chars.length * 2];
        
        for(int i = 0; i < chars.length; i++) {
            bytes[i] = (byte)chars[i];
            bytes[i + 1] = (byte)(chars[i] >> 8);
        }
        
        return bytes;
    }
    
    private final byte[] hash, salt;
    private final byte difficulty;
    
    public Hash(char[] password, int difficulty) {
        salt = new byte[BCrypt.BCRYPT_SALT_LEN];
        new SecureRandom().nextBytes(salt);
        hash = doHash(password, salt, difficulty);
        this.difficulty = (byte)difficulty;
    }
    
    public boolean validatePassword(char[] password) {
        byte[] other = doHash(password, salt, difficulty);
        
        if(hash.length != other.length)
            return false;
        
        int diff = 0;
        
        for(int i = 0; i < hash.length; i++)
            diff |= hash[i] ^ other[i];
        
        return diff == 0;
    }
    
    public byte[] getHash() {
        return hash.clone();
    }
    
    public int getHashDifficulty() {
        return difficulty;
    }
    
    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Hash[");
        
        for(byte b : hash)
            result.append(Integer.toHexString(b));
        
        result.append(", ");
        
        for(byte b : salt)
            result.append(Integer.toHexString(b));
        
        return result.append(']').toString();
    }
    
    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        stream.defaultReadObject();
        validateObject();
    }
    
    @Override
    public void validateObject() throws InvalidObjectException {
        if(hash == null)
            throw new InvalidObjectException("Hash byte array is null.");
        
        if(hash.length == 0)
            throw new InvalidObjectException("Hash byte array is empty.");
        
        if(salt == null)
            throw new InvalidObjectException("Salt byte array is null.");
        
        if(salt.length == 0)
            throw new InvalidObjectException("Salt byte array is empty.");
    }
}
