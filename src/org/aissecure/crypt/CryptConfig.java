/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.crypt;

import org.aissecure.misc.Settings;

public final class CryptConfig implements java.io.Serializable {
    private static final long serialVersionUID = -3265458036756182703L;
    
    public final int cipherSaltSize, cipherIterations, hashDifficulty;
    
    private CryptConfig(int cipherSaltSize, int cipherIterations, int hashDifficulty) {
        this.cipherSaltSize = cipherSaltSize;
        this.cipherIterations = cipherIterations;
        this.hashDifficulty = hashDifficulty;
    }
    
    public static CryptConfig fromSecureData(SecureData data) {
        return new CryptConfig.Builder()
            .setCipherSaltSize(data.getSaltSize())
            .setCipherIterations(data.getIterations())
            .setHashDifficulty(data.getHash().getHashDifficulty())
            .build();
    }
    
    public static CryptConfig fromSettings() {
        return new CryptConfig.Builder()
            .setCipherSaltSize(Settings.get().getCipherSaltSize())
            .setCipherIterations(Settings.get().getCipherIterations())
            .setHashDifficulty(Settings.get().getHashDifficulty())
            .build();
    }
    
    public static final class Builder {
        private int cipherSaltSize, cipherIterations, hashDifficulty;
        
        public Builder setCipherSaltSize(int value) {
            cipherSaltSize = value;
            return this;
        }
        
        public Builder setCipherIterations(int value) {
            cipherIterations = value;
            return this;
        }
        
        public Builder setHashDifficulty(int value) {
            hashDifficulty = value;
            return this;
        }
        
        public CryptConfig build() {
            return new CryptConfig(cipherSaltSize, cipherIterations, hashDifficulty);
        }
    }
}
