/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure;

import static org.aissecure.misc.Util.MIN_PASSWORD_LENGTH;

import org.aissecure.crypt.SecureData;
import org.aissecure.gui.FileOpener;
import org.aissecure.misc.Arguments;
import org.aissecure.misc.FatalExceptionHandler;
import org.aissecure.misc.Settings;
import org.aissecure.misc.Util;

import java.awt.Image;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public final class Launcher {
    private Launcher() {}
    
    public static final String VERSION = "1.3.10";
    public static final Image ICON;
    
    static {
        InputStream stream = Launcher.class.getResourceAsStream("/res/padlock.png");
        Image img;
        
        try {
            img = (stream != null) ? ImageIO.read(stream) : null;
        } catch(IOException ex) {
            img = null;
        }
        
        ICON = img;
    }
    
    public static void main(String[] arguments) {
        Settings.get().getDirectory().mkdir();
        Util.removeCryptographyRestrictions();
        
        Arguments args = new Arguments(arguments);
        
        if(!Settings.get().getLookAndFeel().equalsIgnoreCase("default")) {
            try {
                UIManager.setLookAndFeel(Settings.get().getLookAndFeel());
            } catch(ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
                System.err.println("Look and feel not found: " + Settings.get().getLookAndFeel());
            }
        }
        
        Thread.setDefaultUncaughtExceptionHandler(FatalExceptionHandler.INSTANCE);
        
        if(args.isEmpty()) {
            FileOpener.getInstance().setVisible(true);
        } else if(args.contains('?') || args.contains("help")) {
            System.out.println("Command line usage: [options] \"file name\"");
            System.out.println("Options:");
            System.out.println("-n or --nogui\tDoesn't use the GUI.");
            System.out.println("   This settings applies to -e and -d only.");
            System.out.println("-e or --encrypt\tEncrypts the file from raw data.");
            System.out.println("-d or --decrypt\tDecrypts the file into raw data.");
            System.out.println("-p or --password\tEnters the password for the file in the command line.");
            System.out.println("   The command line usage is -p=[password].");
            System.out.println("-v or --version\tDisplays the version number of this application.");
            System.out.println("-? or --help\tDisplays this text");
        } else if(args.contains('v') || args.contains("version")) {
            System.out.printf("Secure v%s (C) 2015 by Ammon Smith\nSee \"license.txt\" for copyright information.", VERSION);
            System.exit(0);
        } else if((args.contains('e') || args.contains("encrypt"))
               && (args.contains('d') || args.contains("decrypt"))) {
            System.out.println("Conflicting arguments passed: --encrypt and --decrypt");
            System.exit(1);
        } else if(args.contains('e') || args.contains("encrypt")) {
            File file = new File(args.getLast());
            
            if(!file.exists()) {
                System.out.printf("I/O Error: File \"%s\" not found.\n", file);
                System.exit(1);
            }
            
            char[] password, password2;
            if(args.hasShortValue('p')) {
                password = args.getShortValues().get('p').toCharArray();
                
                if(Util.isCommonPassword(password)) {
                    System.out.println("Encryption error: Your password is too common. Choose a more original one.\n");
                    System.exit(1);
                }
                
                if(password.length < MIN_PASSWORD_LENGTH) {
                    System.out.printf("Encryption error: Password must be at least %d characters long.\n", MIN_PASSWORD_LENGTH);
                    System.exit(1);
                }
            } else if(args.hasValue("password")) {
                password = args.getValues().get("password").toCharArray();
                
                if(Util.isCommonPassword(password)) {
                    System.out.println("Encryption error: Your password is too common. Choose a more original one.\n");
                    System.exit(1);
                }
                
                if(password.length < MIN_PASSWORD_LENGTH) {
                    System.out.printf("Encryption error: Password must be at least %d characters long.\n", MIN_PASSWORD_LENGTH);
                    System.exit(1);
                }
            } else if(args.contains('n') || args.contains("nogui")) {
                password = System.console().readPassword("Enter your password: ");
                password2 = System.console().readPassword("Confirm your password: ");

                if(Util.isCommonPassword(password)) {
                    JOptionPane.showMessageDialog(null,
                        "Your password is one of the 10,000 most"
                      + "frequently used passwords. Choose a\n"
                      + "more original (and therefore more\n"
                      + "secure) one.",
                        "Password too common",
                        JOptionPane.WARNING_MESSAGE);
                    System.exit(1);
                }
                
                if(password.length < MIN_PASSWORD_LENGTH) {
                    JOptionPane.showMessageDialog(null, String.format(
                        "Your password must be at least %d characters long.", MIN_PASSWORD_LENGTH),
                        "Password too short",
                        JOptionPane.WARNING_MESSAGE);
                    System.exit(1);
                }
                
                if(!Arrays.equals(password, password2)) {
                    System.out.println("The two passwords do not match.");
                    System.exit(1);
                }
            } else {
                password = Util.getPassword(null, "Enter your password:");
                password2 = Util.getPassword(null, "Confirm your password:");

                if(password.length < MIN_PASSWORD_LENGTH) {
                    JOptionPane.showMessageDialog(null, String.format(
                        "Your password must be at least %d characters long.", MIN_PASSWORD_LENGTH),
                        "Password too short",
                        JOptionPane.WARNING_MESSAGE);
                    System.exit(1);
                }
                
                if(!Arrays.equals(password, password2)) {
                    JOptionPane.showMessageDialog(null,
                        "The two passwords do not match.",
                        "Password mismatch",
                        JOptionPane.ERROR_MESSAGE);
                    System.exit(1);
                }
            }

            SecureData.encryptBinaryFile(file, password);
        } else if(args.contains('d') || args.contains("decrypt")) {
            File file = new File(args.getLast());
            
            if(!file.exists()) {
                System.out.printf("I/O Error: File \"%s\" not found.\n", file);
                System.exit(1);
            }
            
            char[] password;
            if(args.hasShortValue('p'))
                password = args.getShortValues().get('p').toCharArray();
            else if(args.hasValue("password"))
                password = args.getValues().get("password").toCharArray();
            else if(args.contains('n') || args.contains("nogui"))
                password = System.console().readPassword("Enter your password: ");
            else
                password = Util.getPassword(null);

            SecureData.decryptBinaryFile(file, password);
        } else {
            File file = new File(args.getLast());
            
            if(!file.exists()) {
                if(args.contains('n') || args.contains("nogui"))
                    System.out.printf("I/O Error: File \"%s\" not found.\n", file);
                else
                    JOptionPane.showMessageDialog(null,
                        String.format("The system cannot find \"%s\".\nDid you type the file name correctly?", file),
                        "File not found",
                        JOptionPane.ERROR_MESSAGE);
                
                System.exit(1);
            }
            
            if(args.contains('n') || args.contains("nogui"))
                System.out.println("--nogui argument ignored, see --help for information.");

            if(args.hasShortValue('p'))
                FileOpener.getInstance().openEditor(args.getLast(), args.getShortValues().get('p').toCharArray());
            else if(args.hasValue("password"))
                FileOpener.getInstance().openEditor(args.getLast(), args.getValues().get("password").toCharArray());
            else
                FileOpener.getInstance().openEditor(args.getLast());
        }
    }
}
