/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.gui;

import org.aissecure.Launcher;
import org.aissecure.crypt.SecureData;
import org.aissecure.misc.Settings;
import org.aissecure.misc.Util;
import org.aissecure.misc.IncompatibleVersionException;

import java.awt.Component;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.InvalidObjectException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.filechooser.FileFilter;

public final class FileOpener {
    private static FileOpener instance;
    
    public static FileOpener getInstance() {
        if(instance == null)
            instance = new FileOpener();
        
        return instance;
    }
    
    private final JFrame frame;
    private final JPanel panel;
    private final JRadioButton newFile, loadFile;
    private final JButton browse, ok;
    private File loadPath;
    private boolean loadingFile;
    
    @SuppressWarnings("serial")
    private FileOpener() {
        frame = new JFrame("Secure v" + Launcher.VERSION);
        panel = new JPanel();
        newFile = new JRadioButton("New secure text file");
        loadFile = new JRadioButton("Open previous file");
        browse = new JButton("Browse...");
        ok = new JButton("OK");
        
        File path = Settings.get().getLastOpenedFile();
        if(path.exists()) {
            loadPath = path;
            updateSelectedFile();
        }
        
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        
        newFile.setAlignmentX(Component.LEFT_ALIGNMENT);
        newFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                loadingFile = false;
                browse.setEnabled(false);
                ok.setEnabled(true);
                ok.setToolTipText("Creates new secure text file.");
            }
        });
        
        loadFile.setAlignmentX(Component.LEFT_ALIGNMENT);
        loadFile.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                loadingFile = true;
                browse.setEnabled(true);
                ok.setEnabled(loadPath != null);
                ok.setToolTipText("Opens the selected secure text file.");
            }
        });
        
        browse.setEnabled(false);
        browse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                JFileChooser fc = new JFileChooser();
                fc.setCurrentDirectory(
                    loadPath != null && loadPath.exists()
                  ? loadPath
                  : Settings.get().getDirectory());
                fc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isDirectory() ||
                               f.getPath().toLowerCase().endsWith(".crypt");
                    }

                    @Override
                    public String getDescription() {
                        return "Secure text file (.crypt)";
                    }
                });
                
                if(fc.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION)
                    loadPath = fc.getSelectedFile();
                
                ok.setEnabled(loadPath != null);
                updateSelectedFile();
            }
        });
        
        ok.setEnabled(false);
        ok.setAlignmentX(Component.CENTER_ALIGNMENT);
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if(loadingFile) {
                    if(loadPath == null) {
                        JOptionPane.showMessageDialog(frame,
                                "You haven't selected a file.",
                                "Error opening file", JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    
                    openEditor(loadPath, false, null);
                } else {
                    NewFileDialog.getInstance().run();
                }
            }
        });
        
        new ButtonGroup() {{
            add(newFile);
            add(loadFile);
        }};
        
        panel.add(Box.createVerticalStrut(5));
        panel.add(new JPanel() {{
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            add(Box.createHorizontalStrut(5));
            add(newFile);
            add(Box.createHorizontalGlue());  
        }});
        
        panel.add(Box.createVerticalStrut(5));
        panel.add(new JPanel() {{
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            add(Box.createHorizontalStrut(5));
            add(loadFile);
            add(Box.createHorizontalGlue());
            add(browse);
            add(Box.createHorizontalStrut(5));
        }});
        
        panel.add(Box.createVerticalStrut(5));
        panel.add(ok);
        panel.add(Box.createVerticalStrut(5));
        
        frame.setContentPane(panel);
        frame.setIconImage(Launcher.ICON);
        frame.setSize(280, 140);
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowClosing(WindowEvent evt) {
                Settings.get().setLastOpenedFile(loadPath);
                Settings.save();
                frame.dispose();
            }

            public void windowOpened(WindowEvent evt) {}
            public void windowClosed(WindowEvent evt) {}
            public void windowIconified(WindowEvent evt) {}
            public void windowDeiconified(WindowEvent evt) {}
            public void windowActivated(WindowEvent evt) {}
            public void windowDeactivated(WindowEvent evt) {}
        });
        
        placeFrame();
        updateSelectedFile();
        
        if(Launcher.ICON != null)
            frame.setIconImage(Launcher.ICON);
    }
    
    private void updateSelectedFile() {
        loadFile.setText(loadPath == null
             ? "Open previous file"
             : String.format("Open \"%s\"", loadPath.getName()));
        
        frame.setSize(130 + loadFile.getPreferredSize().width, frame.getHeight());
    }
    
    public void openEditor(String path) {
        openEditor(new File(path), true, null);
    }
    
    public void openEditor(String path, char[] password) {
        openEditor(new File(path), true, password);
    }
    
    private void openEditor(File path, boolean openDirectly, char[] password) {
        try {
            boolean readonly = false;
            
            if(path.exists() && !path.canWrite() && Settings.get().getReadonlyWarning()) {
                int result = JOptionPane.showConfirmDialog(frame,
                    "The specified file is set to read-only mode,\n"
                  + "and cannot be edited. Would you like to open\n"
                  + "this file anyway?",
                    "Cannot edit file",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.WARNING_MESSAGE);
                
                if(result != JOptionPane.YES_OPTION)
                    return;
                
                readonly = true;
            }
            
            SecureData data = SecureData.fromFile(path);
            
            if(!data.signatureIsValid())
                JOptionPane.showMessageDialog(frame,
                    "The signature for this file is invalid. Somebody\n"
                  + "may have tampered with this file, or some data\n"
                  + "has become corrupted.\n",
                    "Invalid signature",
                    JOptionPane.WARNING_MESSAGE);
            
            // The argument is null, the user should be prompted
            if(password == null)
                password = Util.getPassword(frame);
            
            // The gui returns null if the user clicked 'cancel'
            if(password == null) {
                if(openDirectly)
                    System.exit(1);
                else
                    return;
            }
            
            Settings.get().setLastOpenedFile(path);
            FileEditor.openInstance(path, password, data.getPlaintext(password), data, readonly, openDirectly);
            frame.setVisible(false);
        } catch(InvalidObjectException | IllegalStateException | EOFException ex) {
            JOptionPane.showMessageDialog(frame,
                "Data is invalid or corrupted:\n" + Util.getExceptionReason(ex),
                "Error decrypting file",
                JOptionPane.ERROR_MESSAGE);
            
            if(openDirectly)
                System.exit(1);
        } catch(IncompatibleVersionException ex) {
            JOptionPane.showMessageDialog(frame,
                "File is of an incompatible version and cannot be read.\n"
              + "Update this application to be able to read this file.\n"
              + "The reported version id for this file is: " + ex.versionId,
                "Error decrypting file",
                JOptionPane.ERROR_MESSAGE);
            
            if(openDirectly)
                System.exit(1);
        } catch(ClassNotFoundException | ClassCastException ex) {
            JOptionPane.showMessageDialog(frame,
                "File is of an incompatible version or is invalid:\n" + Util.getExceptionReason(ex),
                "Error decrypting file",
                JOptionPane.ERROR_MESSAGE);
            
            if(openDirectly)
                System.exit(1);
        } catch(SecurityException ex) {
            JOptionPane.showMessageDialog(frame,
                "Unable to decrypt file:\n" + Util.getExceptionReason(ex),
                "Error decrypting file",
                JOptionPane.ERROR_MESSAGE);
            
            if(openDirectly)
                System.exit(1);
            openEditor(path, openDirectly, null);
        } catch(IOException ex) {
            JOptionPane.showMessageDialog(frame,
                "Error occurred trying to open this file:\n" + Util.getExceptionReason(ex),
                "Error reading from file",
                JOptionPane.ERROR_MESSAGE);
            
            if(openDirectly)
                System.exit(1);
        }
    }
    
    private void placeFrame() {
        GraphicsDevice gd = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
        
        frame.setLocation(
            (gd.getDisplayMode().getWidth() - frame.getWidth()) / 2,
            (gd.getDisplayMode().getHeight() - frame.getHeight()) / 2);
    }
    
    public void setVisible(boolean value) {
        frame.setVisible(value);
    }
}
