/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.gui;

import org.aissecure.misc.Settings;
import org.aissecure.misc.Util;
import org.aissecure.crypt.CryptConfig;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import java.io.File;
import java.io.IOException;

import java.util.Arrays;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.FileFilter;

@SuppressWarnings("serial")
final class NewFileDialog extends JPanel implements Runnable {
    private static NewFileDialog instance;
    
    public static NewFileDialog getInstance() {
        if(instance == null)
            instance = new NewFileDialog();
        
        return instance;
    }
    
    private final JTextField pathField;
    private final JPasswordField passwordField, confirmPassword;
    private final JButton browse;
    private File path;
    
    private NewFileDialog() {
        pathField = new JTextField(8);
        passwordField = new JPasswordField(12);
        confirmPassword = new JPasswordField(12);
        browse = new JButton("Browse...");
        
        pathField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent evt) {
                update();
            }

            @Override
            public void removeUpdate(DocumentEvent evt) {
                update();
            }

            @Override
            public void changedUpdate(DocumentEvent evt) {
                update();
            }
            
            private void update() {
                try {
                    String file = pathField.getText();
                    
                    if(!file.toLowerCase().endsWith(".crypt"))
                        file += ".crypt";

                    if(!file.contains("/") && !file.contains("\\"))
                        file = Settings.get().getDirectory().toString() + "/" + file;
                
                    path = new File(file).getCanonicalFile();
                    pathField.setForeground(Color.BLACK);
                } catch(IOException ex) {
                    pathField.setForeground(Color.RED);
                }
            }
        });
        
        passwordField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                confirmPassword.grabFocus();
            }
        });
        
        browse.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                if(!pathField.getText().isEmpty())
                    Settings.get().setLastOpenedFile(path);
                
                JFileChooser fc = new JFileChooser();
                fc.setCurrentDirectory(Settings.get().getDirectory());
                fc.setFileFilter(new FileFilter() {
                    @Override
                    public boolean accept(File f) {
                        return f.isDirectory() ||
                               f.getPath().toLowerCase().endsWith(".crypt");
                    }

                    @Override
                    public String getDescription() {
                        return "Secure text file (.crypt)";
                    }
                });

                String path = null;
                if(fc.showDialog(NewFileDialog.this, "New File") == JFileChooser.APPROVE_OPTION) {
                    path = fc.getSelectedFile().getPath();

                    if(!path.toLowerCase().endsWith(".crypt"))
                        path += ".crypt";
                }
                
                if(path != null)
                    pathField.setText(path);
            }
        });
        
        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(Box.createVerticalStrut(5));
        this.add(new JPanel() {{
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            add(new JLabel("File name:"));
            add(Box.createHorizontalStrut(5));
            add(pathField);
            add(Box.createHorizontalStrut(5));
            add(browse);
        }});
        
        this.add(Box.createVerticalStrut(5));
        this.add(new JPanel() {{
            setLayout(new GridLayout(2, 2));
            add(new JLabel("New password:"));
            add(passwordField);
            add(new JLabel("Confirm password:"));
            add(confirmPassword);
        }});
    }
    
    @Override
    public void run() {
        run(true);
    }
    
    private void run(boolean resetFields) {
        passwordField.grabFocus();
        
        if(resetFields) {
            passwordField.setText("");
            confirmPassword.setText("");
        }
        
        int result = JOptionPane.showConfirmDialog(this, this,
                "Enter Password", JOptionPane.OK_CANCEL_OPTION);
        
        if(result == JOptionPane.OK_OPTION) {
            char[] password = passwordField.getPassword();
            
            if(!Arrays.equals(password, confirmPassword.getPassword())) {
                JOptionPane.showMessageDialog(this,
                    "The two passwords do not match.",
                    "Invalid password",
                    JOptionPane.WARNING_MESSAGE);
                run(false);
            } else if(password.length < 7) {
                JOptionPane.showMessageDialog(this,
                    "Your password must be at least 7 characters long.",
                    "Password too short",
                    JOptionPane.WARNING_MESSAGE);
                run(false);
            } else if(Util.isCommonPassword(password)) {
                JOptionPane.showMessageDialog(this,
                    "Your password one of the most common 10,000 passwords.\n"
                  + "Choose a more original (and more secure) one.",
                    "Password too insecure",
                    JOptionPane.WARNING_MESSAGE);
                run(false);
            } else {                
                try {
                    path.getCanonicalPath();
                } catch(IOException ex) {
                    JOptionPane.showMessageDialog(this,
                        String.format("The specified file name, \"%s\",", pathField.getText())
                      + "\nis invalid. Choose a different one.",
                        "Cannot create file",
                        JOptionPane.ERROR_MESSAGE);
                    run(false);
                }
                
                if(path.exists()) {
                    if(!path.canWrite()) {
                        JOptionPane.showMessageDialog(this,
                            "The specified file is set to read only\n" +
                            "or you do not have permission to edit here.\n"
                          + "Choose a different file.",
                            "Cannot overwrite file",
                            JOptionPane.ERROR_MESSAGE);
                        return;
                    }
                    
                    result = JOptionPane.showConfirmDialog(this,
                        "This file already exists. Are you sure\n"
                      + "you would like to overwrite it?",
                        "Overwrite file",
                        JOptionPane.YES_NO_OPTION);
                    
                    if(result != JOptionPane.YES_OPTION) {
                        run(false);
                        return;
                    }
                }
                
                FileEditor.openInstance(path, password, CryptConfig.fromSettings());
            }
        }
    }
}
