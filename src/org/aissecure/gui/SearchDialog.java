/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.gui;

import org.apache.commons.lang3.StringEscapeUtils;

import java.awt.Color;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

public final class SearchDialog {
    private static SearchDialog instance;
    
    public static SearchDialog getInstance() {
        if(instance == null)
            instance = new SearchDialog();
        
        return instance;
    }
    
    private static JFrame parent;
    private static JTextPane text;
    
    static void setParentElements(JFrame f, JTextPane t) {
        parent = f;
        text = t;
    }
    
    private final JDialog dialog;
    private final JPanel panel;
    private final JTextField search;
    private final JButton prev, next;
    private final JRadioButton literal, regex;
    private final JCheckBox caseSensitive, matchWord, escapes;
    
    /**
     * Current character index to start searching from.
     */
    private int cursor;
    
    /**
     * Whether the last search yielded a match or not.
     */
    private boolean last;
    
    /**
     * The last direction in which a search was performed.
     * {@code false} - previous
     * {@code true} - next
     */
    private boolean direction;
    
    // regex cache
    private Pattern pattern;
    private Matcher match;
    
    @SuppressWarnings("serial")
    private SearchDialog() {
        dialog = new JDialog(parent, "Search");
        panel = new JPanel();
        search = new JTextField(10);
        prev = new JButton("Previous");
        next = new JButton("Next");
        literal = new JRadioButton("Literal text");
        regex = new JRadioButton("Regular expressions");
        caseSensitive = new JCheckBox("Case sensitive");
        matchWord = new JCheckBox("Match whole word");
        escapes = new JCheckBox("Use escapes (\\n, \\t, \\u...)");
        
        search.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent evt) {
                documentUpdate();
            }

            @Override
            public void removeUpdate(DocumentEvent evt) {
                documentUpdate();
            }

            @Override
            public void changedUpdate(DocumentEvent evt) {
                documentUpdate();
            }
        });
        
        final ActionListener radioListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                documentUpdate();
                matchWord.setEnabled(evt.getSource() != regex);
                
                if(evt.getSource() == regex)
                    getPattern();
            }
        };
        
        final ActionListener checkListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                documentUpdate();
            }
        };
        
        literal.setSelected(true);
        literal.addActionListener(radioListener);
        regex.addActionListener(radioListener);
        prev.addActionListener(new PreviousStringSearcher());
        next.addActionListener(new NextStringSearcher());
        search.addActionListener(new NextStringSearcher());
        caseSensitive.setSelected(true);
        caseSensitive.addActionListener(checkListener);
        escapes.addActionListener(checkListener);
        matchWord.addActionListener(checkListener);
        
        literal.setSelected(true);
        literal.addActionListener(radioListener);
        regex.addActionListener(radioListener);
        prev.addActionListener(new PreviousStringSearcher());
        next.addActionListener(new NextStringSearcher());
        search.addActionListener(new NextStringSearcher());
        caseSensitive.setSelected(true);
        caseSensitive.addActionListener(checkListener);
        escapes.addActionListener(checkListener);
        matchWord.addActionListener(checkListener);
        
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(Box.createVerticalStrut(5));
        panel.add(new JPanel() {{
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            add(Box.createHorizontalStrut(5));
            add(search);
            add(Box.createHorizontalStrut(5));
        }});
        panel.add(Box.createVerticalStrut(5));
        panel.add(new JPanel() {{
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            add(Box.createHorizontalGlue());
            add(prev);
            add(Box.createHorizontalStrut(5));
            add(next);
            add(Box.createHorizontalGlue());
        }});
        panel.add(Box.createVerticalStrut(5));
        panel.add(new JPanel() {{
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            add(new JPanel() {{
                setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
                add(literal);
                add(regex);
            }});
            add(new JPanel() {{
                setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
                add(caseSensitive);
                add(matchWord);
                add(escapes);
            }});
        }});
        
        new ButtonGroup() {{
            add(literal);
            add(regex);
        }};
        
        dialog.setContentPane(panel);
        dialog.setDefaultCloseOperation(WindowConstants.HIDE_ON_CLOSE);
        dialog.setSize(340, 165);
        dialog.setResizable(false);
    }
    
    private abstract class StringSearcher implements ActionListener {
        protected int length;
        private final boolean dir;
        
        private StringSearcher(boolean dir) {
            this.dir = dir;
        }
        
        @Override
        public final void actionPerformed(ActionEvent evt) {
            if(search.getText().isEmpty())
                return;
            
            text.grabFocus();

            if(0 > cursor || cursor >= text.getDocument().getLength())
                cursor = 0;
            
            try {
                if(found()) {
                    text.scrollRectToVisible(text.modelToView(cursor));
                    text.setCaretPosition(cursor);
                    text.moveCaretPosition(cursor + length);
                    search.setForeground(Color.BLACK);
                } else {
                    search.setForeground(Color.RED);
                }
            } catch(BadLocationException ex) {
                System.err.printf("[%d] ", ex.offsetRequested());
                ex.printStackTrace();
                
                RuntimeException e = new IndexOutOfBoundsException(
                        String.format("%s: %d", ex.getMessage(), ex.offsetRequested()));
                e.addSuppressed(ex);
                throw e;
            }
            
            direction = dir;
        }
        
        private boolean found() throws BadLocationException {
            last = regex.isSelected()
                ? regexMatch()
                : (matchWord.isSelected()
                    ? wordMatch()
                    : findText());
            
            return last;
        }
        
        abstract boolean findText() throws BadLocationException;
        abstract boolean findRegex() throws BadLocationException;
        
        private boolean regexMatch() throws BadLocationException {
            if(match == null) {
                if(pattern == null)
                    getPattern();

                // pattern may be null if the regex was invalid
                if(pattern == null)
                    return false;

                match = pattern.matcher(text.getText());
            }
            
            return findRegex();
        }
        
        private boolean wordMatch() throws BadLocationException {
            final String full = text.getText();
            final int prevCursor = cursor, prevLength = length;
            last = true;
            
            while(findText()) {
                boolean left = cursor == 0 || Character.isWhitespace(full.charAt(cursor - 1));
                boolean right = cursor + length == full.length() || Character.isWhitespace(full.charAt(cursor + length));
                
                if(left && right)
                    return true;
            }
            
            cursor = prevCursor;
            length = prevLength;
            
            return false;
        }
        
        final boolean stringEquals(String a, String b) {
            if(!caseSensitive.isSelected()) {
                a = a.toLowerCase();
                b = b.toLowerCase();
            }
            
            return a.equals(b);
        }
    }
    
    private final class PreviousStringSearcher extends StringSearcher {
        PreviousStringSearcher() {
            super(false);
        }
        
        @Override
        boolean findText() throws BadLocationException {
            final Document doc = text.getDocument();
            final String find = getSearchString();
            length = find.length();
            
            if((last || direction) && cursor > 0)
                cursor--;
            
            while(cursor >= 0 && cursor + length <= doc.getLength()) {
                if(stringEquals(find, doc.getText(cursor, length)))
                    return true;

                cursor--;
            }

            return false;
        }
        
        @Override
        boolean findRegex() throws BadLocationException {
            int last = -1, len = 0;
            for(int i = 0; i < cursor; i++) {
                if(match.find(i)) {
                    if(match.start() >= cursor)
                        break;
                    
                    last = match.start();
                    len = match.end() - match.start();
                }
            }
            
            if(last != -1) {
                cursor = last;
                length = len;
                return true;
            }
            
            return false;
        }
    }
    
    private final class NextStringSearcher extends StringSearcher {
        NextStringSearcher() {
            super(true);
        }
        
        @Override
        boolean findText() throws BadLocationException {
            final Document doc = text.getDocument();
            final String find = getSearchString();
            length = find.length();
            
            if((last || !direction) && cursor < doc.getLength() - 1)
                cursor++;
            
            while(cursor + length <= doc.getLength()) {
                if(stringEquals(find, doc.getText(cursor, length)))
                    return true;
                
                cursor++;
            }
            
            return false;
        }
        
        @Override
        boolean findRegex() throws BadLocationException {
            if(match.find()) {
                cursor = match.start();
                length = match.end() - cursor;
                return true;
            }
            
            return false;
        }
    }
    
    private String getSearchString() {
        return escapes.isSelected()
                ? StringEscapeUtils.unescapeJava(search.getText())
                : search.getText();
    }
    
    private void getPattern() {
        try {
            pattern = Pattern.compile(search.getText(),
                caseSensitive.isSelected() ? 0 : Pattern.CASE_INSENSITIVE);
        } catch(PatternSyntaxException ex) {
            search.setForeground(Color.RED);
        }
    }
    
    private void documentUpdate() {
        cursor = 0;
        search.setForeground(Color.BLACK);

        if(!System.lineSeparator().equals("\n"))
            text.setText(text.getText().replace(System.lineSeparator(), "\n"));

        if(regex.isSelected())
            getPattern();
        else
            if(!text.getText().contains(getSearchString()))
                search.setForeground(Color.RED);
    }
    
    public void toggle() {
        if(!dialog.isVisible()) {
            Point loc = parent.getLocation();
            loc.translate(parent.getWidth() + 10, 5);
            dialog.setLocation(loc);
            text.grabFocus();
        }
        
        dialog.setVisible(!dialog.isVisible());
    }
}
