/**
 * Secure Data Manager - Stores text in a AES-encrypted and DSA-signed format.
 * Copyright (C) 2015 Ammon Smith
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.aissecure.gui;

import org.aissecure.Launcher;
import org.aissecure.crypt.CryptConfig;
import org.aissecure.crypt.SecureData;
import org.aissecure.misc.Settings;
import org.aissecure.misc.Util;
import org.masahiko.swing.JFontChooser;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.IOException;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingUtilities;

public final class FileEditor {
    @SuppressWarnings("unused")
    public static void openInstance(File path, char[] password, String plaintext,
    		SecureData data, boolean readonly, boolean openedDirectly) {
    	
        FileEditor instance = new FileEditor(path, password, plaintext, CryptConfig.fromSecureData(data), readonly, openedDirectly);
    }
    
    public static void openInstance(File path, char[] password, CryptConfig config) {
        FileEditor instance = new FileEditor(path, password, "", config, false, false);
        instance.showTips();
    }
    
    private final JFrame frame;
    private final JPanel panel;
    private final JTextPane text;
    private final JScrollPane scroll;
    private final JButton chooseFont, search, saveQuit;
    
    private final File path;
    private final CryptConfig config;
    private final transient String plaintext;
    private final transient char[] password;
    
    @SuppressWarnings("serial")
    private FileEditor(File path, char[] password, String plaintext, CryptConfig config,
    		final boolean readonly, final boolean openedDirectly) {
    	
        this.path = path;
        this.plaintext = plaintext;
        this.password = password;
        this.config = config;
        
        frame = new JFrame(String.format("Secure Data Editor - %s%s", readonly ? "[Read Only] - " : "", path));
        panel = new JPanel();
        text = new JTextPane();
        scroll = new JScrollPane(text);
        chooseFont = new JButton("Choose font...");
        search = new JButton("Search...");
        saveQuit = new JButton("Save and Quit");
        
        text.setText(plaintext);
        text.setEditable(!readonly);
        
        scroll.setAutoscrolls(true);
        
        chooseFont.setToolTipText("Choose the font the editor will use.");
        chooseFont.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                JFontChooser fc = new JFontChooser();
                fc.setSelectedFont(Settings.get().getFont());
                
                if(fc.showDialog(frame) == JFontChooser.OK_OPTION) {
                    Settings.get().setFont(fc.getSelectedFont());
                    text.setFont(fc.getSelectedFont());
                }
            }
        });
        
        search.setToolTipText("Search within the plaintext.");
        search.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                SearchDialog.getInstance().toggle();
            }
        });
        
        saveQuit.setToolTipText("Encrypts and saves all text data.");
        saveQuit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent evt) {
                frame.getSize(Settings.get().getSize());
                if(readonly || save()) {
                    frame.dispose();

                    if(openedDirectly)
                        System.exit(0);
                    else
                        FileOpener.getInstance().setVisible(true);
                }
            }
        });
        
        panel.setLayout(new BorderLayout());
        panel.add(scroll, BorderLayout.CENTER);
        panel.add(new JPanel() {{
            setLayout(new BoxLayout(this, BoxLayout.X_AXIS));
            setAlignmentX(CENTER_ALIGNMENT);
            
            add(Box.createHorizontalGlue());
            add(new JPanel() {{
                setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
                add(Box.createVerticalStrut(5));
                add(chooseFont);
                add(Box.createVerticalStrut(5));
            }});
            
            add(Box.createHorizontalStrut(5));
            add(new JPanel() {{
                setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
                add(Box.createVerticalStrut(5));
                add(search);
                add(Box.createVerticalStrut(5));
            }});
            
            add(Box.createHorizontalStrut(5));
            add(new JPanel() {{
                setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
                add(Box.createVerticalStrut(5));
                add(saveQuit);
                add(Box.createVerticalStrut(5));
            }});
            
        }}, BorderLayout.PAGE_END);
        
        frame.setContentPane(panel);
        frame.setLocationByPlatform(true);
        frame.addWindowListener(new WindowListener() {
            @Override
            public void windowClosing(WindowEvent evt) {
                frame.getSize(Settings.get().getSize());
                if(readonly || save()) {
                    frame.dispose();
                    
                    if(openedDirectly)
                        System.exit(0);
                    else
                        FileOpener.getInstance().setVisible(true);
                }
            }

            public void windowOpened(WindowEvent evt) {}
            public void windowClosed(WindowEvent evt) {}
            public void windowIconified(WindowEvent evt) {}
            public void windowDeiconified(WindowEvent evt) {}
            public void windowActivated(WindowEvent evt) {}
            public void windowDeactivated(WindowEvent evt) {}
        });
        
        if(Launcher.ICON != null)
            frame.setIconImage(Launcher.ICON);
        
        SearchDialog.setParentElements(frame, text);
        text.setFont(Settings.get().getFont());
        frame.setSize(Settings.get().getSize());
        
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                FileOpener.getInstance().setVisible(false);
                frame.setVisible(true);
                
                if(openedDirectly && Settings.get().getDisplayConfirmation()) {
                    String plaintext = text.getText();
                    text.setText("[Plaintext hidden]");
                    JOptionPane.showMessageDialog(frame,
                        "The decrypted plaintext will not be shown\n"
                      + "until you click OK. You can disable this by\n"
                      + "setting \"suppress-display-confirmation\"\n"
                      + "to \"true\" in the configuration file.",
                        "Plaintext hidden",
                        JOptionPane.PLAIN_MESSAGE);
                    text.setText(plaintext);
                }
            }
        });
    }
    
    void showTips() {
        JOptionPane.showMessageDialog(frame,
            "You can now edit the text in your secure file.\n" + 
            "Closing this window will automatically save your data.",
            "Tips", JOptionPane.INFORMATION_MESSAGE);
    }
    
    private boolean save() {
        try {
            if(!text.getText().equals(plaintext)) {
                new SecureData(password, text.getText(), config).saveTo(path);
                text.setText("[Plaintext hidden]");
                if(Settings.get().getEncryptConfirmation())
                    JOptionPane.showMessageDialog(frame,
                        "File saved and encrypted successfully.",
                        "Save successful",
                        JOptionPane.INFORMATION_MESSAGE);
            } else if(Settings.get().doFileCloseConfirmation()) {
                JOptionPane.showMessageDialog(frame,
                        "File closed successfully, no changes were made.",
                        "Save successful",
                        JOptionPane.INFORMATION_MESSAGE);
            }

            Settings.save();
            return true;
        } catch(IOException ex) {
            ex.printStackTrace();
            return JOptionPane.showConfirmDialog(frame,
                "Error saving data to stream:\n" + Util.getExceptionReason(ex)
              + "\n\nPress cancel to quit without saving.",
                "Save failed",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.ERROR_MESSAGE) == JOptionPane.CANCEL_OPTION;
        } catch(IllegalStateException ex) {
            ex.printStackTrace();
            return JOptionPane.showConfirmDialog(frame,
                "Password data internally corrupted:\n" + Util.getExceptionReason(ex)
              + "\n\nPress cancel to quit without saving.",
                "Save failed", 
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.ERROR_MESSAGE) == JOptionPane.CANCEL_OPTION;
        }
    }
}
