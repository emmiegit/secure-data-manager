#!/usr/bin/env/python
import hashlib, os, traceback, zipfile

exe_format = """\
Please take some time to review the hash sums for "secure.exe" in the installation folder.

MD5:  [ %(md5)s ]
SHA1: [ %(sha1)s ]
SHA256: [ %(sha256)s ]

If any of the sums are incorrect, please contact me immediately and cease using
the application until the issue is resolved, as it is possible that somebody has
tampered with the files.
"""

jar_format = """\
Please take some time to review the hash sums for "SecureTextManager.jar".

MD5: [ %(md5)s ]
SHA1: [ %(sha1)s ]
SHA256: [ %(sha256)s ]

If any of the sums are incorrect, please contact me immediately and cease using
the application until the issue is resolved, as it is possible that somebody has
tampered with the files.
"""

def get_hashes(path):
    raw = file(path, 'rb').read()
    
    return {
        "md5": hashlib.md5(raw).hexdigest(),
        "sha1": hashlib.sha1(raw).hexdigest(),
        "sha256": hashlib.sha256(raw).hexdigest(),
    }

if __name__ == "__main__":
    version = raw_input("Enter version number: ")
    
    try:
        file("checksums.txt", 'w+').write(exe_format % get_hashes("dist/secure.exe"))
        file("dist/checksums.txt", 'w+').write(jar_format % get_hashes("dist/SecureTextManager.jar"))
        print "Updated hashes successfully."
    except:
        print "Could not update hashes:\n"
        traceback.print_exc()
    
    try:
        zip = zipfile.ZipFile("dist/Secure_%s.zip" % version, 'w')
        zip.write("license.txt", "license.txt", zipfile.ZIP_DEFLATED)
        zip.write("dist/SecureTextManager.jar", "SecureTextmanager.jar", zipfile.ZIP_DEFLATED)
        zip.write("dist/checksums.txt", "checksums.txt", zipfile.ZIP_DEFLATED);
        zip.write("dist/secure-data-manager.sh", "secure-data-manager.sh", zipfile.ZIP_DEFLATED);
        zip.close()
        print "Created zip distro successfully."
    except:
        print "Could not create linux zip distro:"
        traceback.print_exc()
    
    try:
        new = ""
        
        for line in file("secure-install.iss", 'r').readlines():
            if line.startswith("AppVersion="):
                new += "AppVersion=%s\n" % version
            elif line.startswith("OutputBaseFilename="):
                new += "OutputBaseFilename=SecureTextManager_Setup_%s\n" % version
            else:
                new += line
        
        file("secure-install.iss", 'w+').write(new)
        print "Updated \"secure-install.iss\" successfully."
    except:
        print "Could not update \"secure-install.iss\":"
        traceback.print_exc()
    
    os.system("pause")
